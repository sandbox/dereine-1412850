<?php

/**
 * Define the current api version of drush stats.
 */
define('DRUSH_STATS_API_VERSION', '0.1');

function drush_stats_drush_command() {
  $items = array();

  $items['drush-stats-raw'] = array(
    'description' => "Print json file.",
    'aliases' => array('ds-raw'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
    // 'callback arguments' => array(drush_stats_filename()),
  );

  $items['drush-stats-overall'] = array(
    'description' => "Print overall stat.",
    'aliases' => array('ds-oa'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
    // 'callback arguments' => array(drush_stats_filename()),
    'options' => array(
      'filter' => 'Regex fun.',
    ),
    'examples' => array(
      'drush ds-oa' => 'All time overview',
      'drush ds-oa --filter="2012"' => 'Overview for 2011',
      'drush ds-oa --filter="/2011:12/"' => 'Overview for 2011-12',
      'drush ds-oa --filter="/core/"' => 'Core only',
    ),
  );

  $items['drush-stats-clear'] = array(
    'description' => 'Clears drush stats',
    'aliases' => array('dsc'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  $items['drush-stats-upgrade'] = array(
    'description' => 'Upgrade stats stat file.',
    'aliases' => array('ds-up'),
    'bootstrap' => DRUSH_BOOTSTRAP_DRUSH,
  );

  return $items;
}

function drush_stats_read($force = FALSE) {
  $stats = json_decode(file_get_contents(drush_stats_filename()), TRUE);
  if (isset($stats['api-version']) || $stats['api-version'] === DRUSH_STATS_API_VERSION || $force == TRUE || empty($stats)) {
    return $stats;
  }

  drush_set_error('Drush Stats API Version is wrong. Please run drush ds-up.');
  drush_die();
}

function drush_drush_stats_raw() {
  drush_print_r(drush_stats_read());
}

function drush_drush_stats_overall() {
  $stats = drush_stats_read();
  drush_print_r("API Version: " . $stats['api-version']) . PHP_EOL;

  if (!isset($stats['commands'])) {
    drush_set_error('No stats available. Please try again now ...');
    return;
  }

  $rows = array(
    array('#', 'Command', 'Count',),
  );

  // Filter stuff.
  $filter = drush_get_option('filter');
  if (!empty($filter)) {
    $filtered_keys = preg_grep($filter, array_keys($stats['commands']));
    $stats['commands'] = array_intersect_key($stats['commands'], array_flip($filtered_keys));
  }

  $data = array();
  foreach($stats['commands'] as $key => $count) {
    $item = explode(':', $key);
    $command = reset($item);
    $data[$command] = isset($data[$command]) ? $data[$command] + $count : $count;
  }

  asort($data);
  arsort($data);

  $i = 1;
  foreach($data as $key => $count) {
    $rows[] = array($i, $key, $count);
    $i++;
  }

  drush_print_table($rows, TRUE);
  drush_print('Sum: ' . array_sum($data));
}

function drush_drush_stats_upgrade() {
  if (drush_confirm('Update stats file. Are you really sure?')) {
    $stats = drush_stats_read(TRUE);
    $new_stats = array('api-version' => 0.1);
    $upgrade = FALSE;

    if (isset($stats[':api_version']) && $stats[':api_version'] < 0.1) {
      foreach ($stats as $command => $count) {
        $item = explode(':', $command);
        if (count($item) == 5) {
          unset($item[3]);
          $date = implode(':', $item);
          $new_stats['commands'][$date] = $count;
        }
      }
      $upgrade = TRUE;
    }

    if ($upgrade) {
      $json = json_encode($new_stats);
      file_put_contents(drush_stats_filename(), $json);
    }
  }
}

/**
 * Map reduce.
 */
function drush_stats_rsum($v, $w) {
  if (is_array($w)) {
    $v += array_reduce($w, 'drush_stats_rsum');
  }
  else {
    $v += $w;
  }

  return $v;
}

function drush_drush_stats_clear() {
  if (drush_confirm('God kills a kitten if you clear the drush stats. Are you really sure?')) {
    $filename = drush_stats_filename();
    for ($i = 0; $i < rand(2, 4); $i++) {
      if (drush_confirm('are ye really sure?')) {
      }
      else {
        return;
      }
    }
    file_delete($filename);
  }
}
function drush_stats_drush_init() {
  $command = drush_parse_command();
  $command_name = $command['command'];

  drush_stats_add($command_name);
}

function drush_stats_filename() {
  // @todo: configuration?
  return $_SERVER['HOME'] . '/.drush/drush_stats.json';
}

function drush_stats_add($command) {
  $filename = drush_stats_filename();
  if (!file_exists($filename)) {
    $data = array();
  }
  else {
    $json = file_get_contents($filename);
    $data = json_decode($json, TRUE);
  }

  $groups = array();
  $groups[] = $command;
  $groups[] = date('Y');
  $groups[] = date('m');
  $groups[] = date('d');

  $implode = implode(':', $groups);
  if (isset($data['commands'][$implode])) {
    $data['commands'][$implode]++;
  } else {
    $data['commands'][$implode] = 1;
  }

  // Add api version.
  $data['api-version'] = DRUSH_STATS_API_VERSION;

  $json = json_encode($data);
  file_put_contents($filename, $json);
}

